import React, {useEffect} from 'react';
import './Counter.css';
import {useDispatch, useSelector} from "react-redux";
import {add, decrement, fetchCounter, increment, putCounter, subtract} from "../../store/actions";

const Counter = () => {
  const dispatch = useDispatch();
  const state = useSelector(state => state);

  useEffect(() => {
      dispatch(fetchCounter());
  }, [dispatch]);

  useEffect(() => {
      dispatch(putCounter(state.counter));
  });


  const increaseCounter = () => dispatch(increment());
  const decreaseCounter = () => dispatch(decrement());
  const plusCounter = () => dispatch(add(5));
  const minusCounter = () => dispatch(subtract(5));

  return (
    <div className="Counter">
      <p>Counter:</p>
      <h1>{state.counter}</h1>
      <button onClick={increaseCounter}>Increase</button>
      <button onClick={decreaseCounter}>Decrease</button>
      <button onClick={plusCounter}>Increase by 5</button>
      <button onClick={minusCounter}>Decrease by 5</button>
    </div>
  );
};

export default Counter;