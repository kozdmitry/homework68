import {
  ADD,
  DECREMENT,
  FETCH_COUNTER_FAILURE,
  FETCH_COUNTER_REQUEST,
  FETCH_COUNTER_SUCCESS, FETCH_TEXT_FAILURE, FETCH_TEXT_REQUEST, FETCH_TEXT_SUCCESS,
  INCREMENT,
  SUBTRACT
} from "./actions";
import {INPUT} from "../storeToDo/actionsToDo";

const initialState = {
  counter: 0,
  loading: true,
  error: false,
  input: "",
  list: null,
  id: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return {...state, counter: state.counter + 1};
    case DECREMENT:
      return {...state, counter: state.counter - 1};
    case ADD:
      return {...state, counter: state.counter + action.value};
    case SUBTRACT:
      return {...state, counter: state.counter - action.value};
    case FETCH_COUNTER_REQUEST:
      return {...state, loading: true, error: false};
    case FETCH_COUNTER_SUCCESS:
      return {...state, loading: false, counter: action.counter};
    case FETCH_COUNTER_FAILURE:
      return {...state, loading: false, error: true};
    case FETCH_TEXT_REQUEST:
      return {...state, loading: true, error: false};
    case FETCH_TEXT_SUCCESS:
      return {...state, loading: false, list: action.list};
    case FETCH_TEXT_FAILURE:
      return {...state, loading: false, error: true};
    default:
      return state;
  }
};

export default reducer;