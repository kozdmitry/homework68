import React from 'react';
import Counter from "./containers/Counter/Counter";
import ToDo from "./containers/ToDo/ToDo";


const App = () => (
    <>
        <Counter/>
        <ToDo/>
    </>
    );
export default App;

