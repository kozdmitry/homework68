import React, {useEffect, useState} from 'react';
import "./ToDo.css";
import {useDispatch, useSelector} from "react-redux";
import {delTextToDo, fetchTextToDo, newTextTodo, putCounter} from "../../store/actions";

const ToDo = () => {
    const dispatch = useDispatch();
    const list = useSelector((state) => state.list);

    const [value, setValue] = useState('')

    console.log(list);

    useEffect(() => {
        dispatch(fetchTextToDo());
    }, [dispatch]);

    const del = (id) => {
        delTextToDo(id);
    };

    // useEffect(() => {
    //     dispatch(fetchTextToDo(value));
    // });


    const post = (e) => {
        e.preventDefault();
        const obj = {
            message: value
        }
        dispatch(newTextTodo(obj));
        console.log('post');
    }

    // const input = (e) => dispatch({type: INPUT, value: e.target.value})
    // console.log(value);
    const input = (e) => {
        setValue(e.target.value)
    }

    return (
        <div className="ToDo">
            ToDo list
            <form onSubmit={post}>
            <input type="text"
                   value={value}
                   onChange={e => (input(e))}/>
            <button type={'submit'}>add</button>
            </form>
            <div>
                {list && Object.keys(list).map(key => (
                    <div className="text" key={key}>
                        <h2>{list[key].message}</h2>
                        <button className="btn" onClick={del}>X</button>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default ToDo;