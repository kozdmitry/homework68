import axios from "axios";

export  const INCREMENT = 'INCREMENT';
export  const DECREMENT = 'DECREMENT';
export  const ADD = 'ADD';
export  const SUBTRACT = 'SUBTRACT';
export  const DEL = 'DEL';
export  const POST = 'POST';
export  const GET = 'GET';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const FETCH_TEXT_REQUEST = 'FETCH_TEXT_REQUEST';
export const FETCH_TEXT_SUCCESS = 'FETCH_TEXT_SUCCESS';
export const FETCH_TEXT_FAILURE = 'FETCH_TEXT_FAILURE';


export const increment = () => ({type: INCREMENT});
export const decrement = () => ({type: DECREMENT});
export const add = value => ({type: ADD, value});
export const subtract = value => ({type: SUBTRACT, value});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});


export const del = () => ({type: DEL});
export const post = () => ({type: POST});
export const get = text => ({type: GET, text});


export const fetchTextRequest = () => ({type: FETCH_TEXT_REQUEST});
export const fetchTextSuccess = list => ({type: FETCH_TEXT_SUCCESS, list});
export const fetchTextFailure = () => ({type: FETCH_TEXT_FAILURE});

export const fetchCounter = () => {
    return async dispatch => {
        dispatch(fetchCounterRequest());

        try {
            const response = await axios.get ('https://js9-hw65-default-rtdb.firebaseio.com/counter.json');
            dispatch(fetchCounterSuccess(response.data))
        } catch (e) {
            dispatch(fetchCounterFailure());
        }
    };
};

export const putCounter = (state) => {
    return async () => {
        try {
            await axios.put ('https://js9-hw65-default-rtdb.firebaseio.com/counter.json', state);
        } catch (e) {
            console.log('error')
        }
    };
};


export const fetchTextToDo = () => {
    return async dispatch => {
        dispatch(fetchTextRequest());
        try {
            const resp = await axios.get ('https://js9-hw65-default-rtdb.firebaseio.com/list.json');
            dispatch(fetchTextSuccess(resp.data))
        } catch (e) {
            dispatch(fetchTextFailure());
        }
    };
};

export const newTextTodo = (data) => {
    return async (dispatch) => {
        try {
            await axios.post('https://js9-hw65-default-rtdb.firebaseio.com/list.json', data);
            dispatch(fetchTextToDo());
        } catch (e) {
            console.log(e);
        }
    }
};

export const delTextToDo = (id) => {
    return async (dispatch) => {
        await axios.del ('https://js9-hw65-default-rtdb.firebaseio.com/list.json', id);
        dispatch(fetchTextToDo(id));
    };
};
